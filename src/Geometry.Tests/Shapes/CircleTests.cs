﻿using System;
using Geometry.Shapes;
using NUnit.Framework;

namespace Geometry.Tests.Shapes
{
    public class CircleTests
    {
        [Test]
        [TestCase(0, 0)]
        [TestCase(1, Math.PI)]
        [TestCase(2.5, 19.634954)]
        public void GetArea_Returns_Correct_Results(double radius, double expectedArea)
        {
            var circle = new Circle(radius);
            
            Assert.AreEqual(expectedArea, circle.GetArea(), 0.0000001);
        }

        [Test]
        public void Throws_ArgumentOutOfRangeException_For_Negative_Radius()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new Circle(-1.0));
        }
    }
}