﻿using System;
using Geometry.Shapes;
using NUnit.Framework;
using NUnit.Framework.Internal.Commands;

namespace Geometry.Tests.Shapes
{
    public class TriangleTests
    {
        [Test]
        [TestCase(10, 12, 15, 59.8116836, Description = "Triangle")]
        [TestCase(2, 2, 2.8284271247461903, 2, Description = "Right Triangle")]
        [TestCase(4, 2.5, 2.5, 3, Description = "Isosceles Triangle")]
        [TestCase(2.5, 2.5, 2.5, 2.7063293868263707, Description = "Equilateral Triangle")]
        [TestCase(.5, .5, .7, .12497499749949989)]
        public void GetArea_Returns_Correct_Results(double a, double b, double c, double expectedArea)
        {
            var triangle = new Triangle(a, b, c);

            Assert.AreEqual(expectedArea, triangle.GetArea(), 0.0000001);
        }

        [Test]
        public void Constructor_Throws_ArgumentOutOfRangeException_For_Invalid_Triangle_Sides()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new Triangle(4, 4, 8));
        }

        [Test]
        public void Constructor_Throws_ArgumentOutOfRangeException_For_Negative_Triangle_Side()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new Triangle(-10, -12, -15));
        }

        [Test]
        public void Constructor_Creates_Triangle_For_Valid_Triangle_Values()
        {
            var sut = new Triangle(10, 12, 15);
            Assert.IsFalse(sut.IsIsoscelesTriangle());
            Assert.IsFalse(sut.IsEquilateralTriangle());
        }

        [Test]
        public void Constructor_Creates_Equilateral_Triangle_Using_Same_Side()
        {
            var sut = new Triangle(7);

            Assert.IsTrue(sut.IsEquilateralTriangle());
            Assert.IsTrue(sut.IsIsoscelesTriangle());
        }

        [Test]
        public void Constructor_Creates_Isosceles_Triangle_Using_Two_Sides_And_Base()
        {
            var sut = new Triangle(7, 2);

            Assert.IsTrue(sut.IsIsoscelesTriangle());
            Assert.IsFalse(sut.IsEquilateralTriangle());
        }

        [Test]
        public void IsRight_Returns_True_For_Right_Triangle()
        {
            var cathetus1 = 2;
            var cathetus2 = 2;
            var hypotenuse = Math.Sqrt(Math.Pow(cathetus1, 2) + Math.Pow(cathetus2, 2));

            var sut = new Triangle(cathetus1, cathetus2, hypotenuse);

            Assert.IsTrue(sut.IsRight());
        }

        [Test]
        public void IsRight_Returns_False_For_NonRight_Triangle()
        {
            var sut = new Triangle(10, 12, 15);

            Assert.IsFalse(sut.IsRight());
        }
    }
}