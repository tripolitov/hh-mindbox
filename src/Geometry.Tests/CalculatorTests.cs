using System;
using System.Collections;
using Geometry.Shapes;
using NUnit.Framework;

namespace Geometry.Tests
{
    public class CalculatorTests
    {
        [TestCaseSource(nameof(ShapeCases))]
        public void CalculateArea(IShape shape, double expectedArea)
        {
            var sut = new Calculator();
            Assert.AreEqual(expectedArea, sut.CalculateAreaFor(shape), 0.0000001);
        }

        public static IEnumerable ShapeCases
        {
            get
            {
                yield return new object[] {new Circle(0), 0};
                yield return new object[] {new Circle(1), Math.PI};
                yield return new object[] {new Circle(2.5), 19.634954};
                yield return new object[] { new Triangle(10, 12, 15), 59.8116836 };
                yield return new object[] { new Triangle(2, 2, 2.8284271247461903), 2 };
                yield return new object[] { new Triangle(2.5, 4), 3 };
                yield return new object[] { new Triangle(2.5), 2.7063293868263707 };
            }
        }
    }
}