﻿namespace Geometry
{
    public class Calculator
    {
        public double CalculateAreaFor(IShape shape)
        {
            return shape.GetArea();
        }
    }
}
