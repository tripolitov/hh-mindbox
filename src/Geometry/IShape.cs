﻿namespace Geometry
{
    /// <summary>
    /// Describes a shape for which area can be calculated
    /// </summary>
    public interface IShape
    {
        /// <summary>
        /// Calculates shape Area
        /// </summary>
        double GetArea();
    }
}