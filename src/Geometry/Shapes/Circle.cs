﻿using System;

namespace Geometry.Shapes
{
    /// <summary>
    /// Describes Circle shape using radius
    /// </summary>
    public class Circle : IShape
    {
        private readonly double radius;

        /// <summary>
        /// Creates circle.
        /// </summary>
        /// <remarks>
        /// It is assumed that negative radius is an invalid value for a Circle
        /// </remarks>
        public Circle(double radius)
        {
            if (radius < 0) throw new ArgumentOutOfRangeException(nameof(this.radius));

            this.radius = radius;
        }

        /// <summary>
        /// Calculates Circle Area
        /// </summary>
        public double GetArea()
        {
            return Math.PI * Math.Pow(this.radius, 2);
        }

        public override string ToString()
        {
            return $"{nameof(Circle)} {{ {nameof(this.radius)}: {this.radius} }}";
        }
    }
}