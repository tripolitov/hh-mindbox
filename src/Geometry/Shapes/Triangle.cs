﻿using System;

namespace Geometry.Shapes
{
    /// <summary>
    /// Describes Triangle shape using three sides
    /// </summary>
    public class Triangle : IShape
    {
        private readonly double a;
        private readonly double b;
        private readonly double c;

        /// <summary>
        /// Creates Equilateral Triangle
        /// </summary>
        /// <param name="side"></param>
        public Triangle(double side) : this(side, side, side)
        {
        }

        /// <summary>
        /// Creates Isosceles Triangle
        /// </summary>
        public Triangle(double side, double @base) : this(side, side, @base)
        {
        }

        /// <summary>
        /// Creates Triangle
        /// </summary>
        public Triangle(double a, double b, double c)
        {
            if (!isValidTriangle(a, b, c))
            {
                throw new ArgumentOutOfRangeException();
            }

            this.a = a;
            this.b = b;
            this.c = c;
        }

        /// <summary>
        /// Validates that provided sides can form a valid triangle
        /// </summary>
        private static bool isValidTriangle(double a, double b, double c)
        {
            return a + b > c && a + c > b && b + c > a;
        }

        /// <summary>
        /// Calculates Triangle Area
        /// </summary>
        public double GetArea()
        {
            // using Heron formula
            // 1. calculate half perimeter
            var p = GetPerimeter() / 2;
            // 2. calculate area
            return Math.Sqrt(p * (p - this.a) * (p - this.b) * (p - this.c));
        }

        /// <summary>
        /// Calculates Triangle perimeter
        /// </summary>
        /// <returns></returns>
        public double GetPerimeter()
        {
            return this.a + this.b + this.c;
        }

        /// <summary>
        /// Returns true if Triangle is Right
        /// </summary>
        public bool IsRight()
        {
            var hypotenusePow2 = Math.Pow(Math.Max(Math.Max(this.a, this.b), this.c), 2);
            var sumOfSidesPow2 = Math.Pow(this.a, 2) + Math.Pow(this.b, 2) + Math.Pow(this.c, 2);
            return Math.Abs(hypotenusePow2 - (sumOfSidesPow2 - hypotenusePow2)) < 0.0001;
        }

        /// <summary>
        /// Returns true if Triangle is Equilateral
        /// </summary>
        public bool IsEquilateralTriangle()
        {
            return Math.Abs(this.a - this.b) < 0.0001
                   && Math.Abs(this.a - this.c) < 0.0001
                   && Math.Abs(this.b - this.c) < 0.0001;
        }

        /// <summary>
        /// Returns true if Triangle is Isosceles
        /// </summary>
        public bool IsIsoscelesTriangle()
        {
            return Math.Abs(this.a - this.b) < 0.0001
                   || Math.Abs(this.a - this.c) < 0.0001
                   || Math.Abs(this.b - this.c) < 0.0001;
        }

        public override string ToString()
        {
            return $"{nameof(Triangle)} {{ {nameof(this.a)}: {this.a} {nameof(this.b)}: {this.b} {nameof(this.c)}: {this.c} }}";
        }
    }
}