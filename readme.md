# Задание
Напишите библиотеку для поставки внешним клиентам, которая умеет вычислять 
1. площадь круга по радиусу 
1. треугольника по трем сторонам

Дополнительно к работоспособности оценим:
- Юнит-тесты
- Легкость добавления других фигур
- Вычисление площади фигуры без знания типа фигуры 
- Проверку на то, является ли треугольник прямоугольным

# Shapes
## Circle
### Create
To create a circle shape provide radius value to the constructor of `Circle` class:
```
var circle = new Circle(radius:17);
```
### Get Area
To get area of a circle call GetArea method
```
var area = circle.GetArea();
```
## Triangle
### Create
To create a trangle shape provide length values of all sides to one of the constructors of `Triangle` class:
For Equilateral triangle use
```
var equilateral = new Triangle(side:17);
```
For Equilateral triangle use
```
var isosceles = new Triangle(side:17, base: 2);
```
For Scalene triangle use
```
var isosceles = new Triangle(10, 12, 15);
```
### Get Area
To get area of a triangle call GetArea method
```
var area = triangle.GetArea();
```
# Creating additional shape
To add shape a class which implements `IShape` interface should be created.
For example to create a square you can use following code
```
public class Square : IShape
{
    private readonly double side;

    public Square(double side)
    {
        if (side <= 0) throw new ArgumentOutOfRangeException(nameof(side));
        this.side = side;
    }

    public double GetArea()
    {
        return Math.Pow(side, 2);
    }
}
```