select p.name as product_name, COALESCE(c.name, 'NO CATEGORY') as category_name
  from public.products as p
    left join public.products_categories as pc on p.id = pc.product_id
    left join public.categories as c on pc.category_id = c.id
  order by c.name
;

/*
> psql -U postgres -d hh-mindbox -f query.sql
 product_name | category_name
--------------+---------------
 A            | C1
 B            | C2
 D            | C2
 A            | C2
 C            | C2
 E            | C3
 F            | NO CATEGORY
(7 rows)
*/