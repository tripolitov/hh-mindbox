CREATE TABLE public.products
(
  "id" serial NOT NULL,
  "name" character varying(64) NOT NULL,
  CONSTRAINT "PK_products_id" PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE public.categories
(
  "id" serial NOT NULL,
  "name" character varying(64) NOT NULL,
  CONSTRAINT "PK_categories_id" PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE TABLE IF NOT EXISTS public.products_categories (
    product_id integer NOT NULL,
    category_id integer NOT NULL,
    CONSTRAINT "PK_products_categories_product_id_category_id" PRIMARY KEY (product_id, category_id),
    CONSTRAINT "FK_products_categories_product_id" FOREIGN KEY (product_id)
      REFERENCES public.products (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
    CONSTRAINT "FK_products_categories_category_id" FOREIGN KEY (category_id)
      REFERENCES public.categories (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE  
)
WITH (
  OIDS=FALSE
);
