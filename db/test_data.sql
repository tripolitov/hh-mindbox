insert into public.products(name)
  select 'A' as "name" 
  union select 'B' 
  union select 'C'
  union select 'D'
  union select 'E'
  union select 'F'
;

insert into public.categories(name)
  select 'C1' as "name" 
  union select 'C2' 
  union select 'C3'
;

insert into public.products_categories(product_id, category_id)
  select p.id, c.id from public.products as p, public.categories c where p.name = 'A' AND c.name = 'C1'
  union select p.id, c.id from public.products as p, public.categories c where p.name = 'A' AND c.name = 'C2'
  union select p.id, c.id from public.products as p, public.categories c where p.name = 'B' AND c.name = 'C2'
  union select p.id, c.id from public.products as p, public.categories c where p.name = 'C' AND c.name = 'C2'
  union select p.id, c.id from public.products as p, public.categories c where p.name = 'D' AND c.name = 'C2'
  union select p.id, c.id from public.products as p, public.categories c where p.name = 'E' AND c.name = 'C3'
;