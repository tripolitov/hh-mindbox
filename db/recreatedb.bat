@cls
SET userName=postgres
SET dbName=hh-mindbox

@rem Kill existing connections
psql -U %userName% -d "postgres" -c "SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity WHERE pg_stat_activity.datname = '%dbName%' AND pid <> pg_backend_pid();"

dropdb --if-exists -U %userName% %dbName%
@if %errorlevel% neq 0 exit /b %errorlevel%
createdb -U %userName% %dbName%
@if %errorlevel% neq 0 exit /b %errorlevel%

psql -U %userName% -d %dbName% -f create_tables.sql -v "ON_ERROR_STOP=1"
echo %errorlevel%
@if %errorlevel% neq 0 exit /b %errorlevel%

psql -U %userName% -d %dbName% -f test_data.sql -v "ON_ERROR_STOP=1"
echo %errorlevel%
@if %errorlevel% neq 0 exit /b %errorlevel%

psql -U %userName% -d %dbName% -f query.sql -v "ON_ERROR_STOP=1"
echo %errorlevel%
@if %errorlevel% neq 0 exit /b %errorlevel%